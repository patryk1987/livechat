﻿class Generators {
  name() {
    let name = "";
    let letters = "abcdefghijklmnopqrstuvwxyz".split("");
    while (name.length < 5) {
      var letter = letters[Math.floor(Math.random() * letters.length)];
      if (name.length === 0) {
        name += letter.toUpperCase();
      } else {
        name += letter;
      }
    }
    return name;
  }

  uuid() {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
      var r =
          (parseFloat(
            "0." +
              Math.random()
                .toString()
                .replace("0.", "") +
              new Date().getTime()
          ) *
            16) |
          0,
        v = c == "x" ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }

  sentence() {
    let nouns = [
      "bird",
      "clock",
      "boy",
      "plastic",
      "duck",
      "teacher",
      "old lady",
      "professor",
      "hamster",
      "dog"
    ];
    let verbs = [
      "kicked",
      "ran",
      "flew",
      "dodged",
      "sliced",
      "rolled",
      "died",
      "breathed",
      "slept",
      "killed"
    ];
    let adjectives = [
      "beautiful",
      "lazy",
      "professional",
      "lovely",
      "dumb",
      "rough",
      "soft",
      "hot",
      "vibrating",
      "slimy"
    ];
    let adverbs = [
      "slowly",
      "elegantly",
      "precisely",
      "quickly",
      "sadly",
      "humbly",
      "proudly",
      "shockingly",
      "calmly",
      "passionately"
    ];

    let slices = [];
    for (let i = 0; i < 4; i++) {
      slices.push(Math.floor(Math.random() * 10));
    }

    return (
      "The " +
      adjectives[slices[0]] +
      " " +
      nouns[slices[1]] +
      " " +
      adverbs[slices[2]] +
      " " +
      verbs[slices[3]] +
      "."
    );
  }
}

module.exports = new Generators();

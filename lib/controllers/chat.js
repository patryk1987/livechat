﻿const room = require("./room");

let join = (req, res, next) => {
  try {
    const { name, roomOrder } = req.body;
    let id = room.addHumanUser(name, roomOrder);
    res.json({
      id
    });
  } catch (err) {
    next(err);
  }
};

let message = (req, res, next) => {
  try {
    const { name, message } = req.body;
    let messages = room.sendHumanMessage(name, message);
    res.json({
      messages
    });
  } catch (err) {
    next(err);
  }
};

let getData = (res, next) => {
  try {
    let usersToSend = [];
    let usersSource = room.users;
    for (let i = 0; i < usersSource.length; i++) {
      let user = (({ id, name, roomId }) => ({ id, name, roomId }))(
        usersSource[i]
      );
      usersToSend.push(user);
    }

    res.json({
      users: usersToSend,
      rooms: room.rooms
    });
  } catch (err) {
    next(err);
  }
};

module.exports = {
  join,
  message,
  getData
};

﻿const config = require("../../config");
const generators = require("./generators");

class Room {
  constructor() {
    this.usersCount = config.usersCount;
    this.roomsCount = Math.ceil(this.usersCount / 4);
    this.users = [];
    this.rooms = [];
  }

  addRooms() {
    for (let i = 0; i < this.roomsCount; i++) {
      let id = generators.uuid();
      this.rooms.push({ id, order: i, messages: [] });
    }
  }

  findRoom(roomId) {
    for (let i = 0; i < this.rooms.length; i++) {
      if (this.rooms[i].id === roomId) {
        return i;
      }
    }
  }

  findRoomByOrder(order) {
    for (let i = 0; i < this.rooms.length; i++) {
      if (this.rooms[i].order === order) {
        return i;
      }
    }
  }

  findUserIndexByName(name) {
    for (let i = 0; i < this.users.length; i++) {
      if (this.users[i].name === name) {
        return i;
      }
    }
    return false;
  }

  addUsers() {
    for (let i = 0; i < this.usersCount; i++) {
      let _self = this;
      let id = generators.uuid();
      let name = generators.name();
      this.users.push({
        id,
        name,
        roomId: undefined,
        switchClockId: undefined,
        switchRooms: function() {
          this.switchClockId = setInterval(() => {
            let newRoom =
              _self.rooms[Math.floor(Math.random() * _self.rooms.length)];
            this.roomId = newRoom.id;
          }, 1000 * 10);
        },
        messageClockId: undefined,
        sendMessage: function() {
          this.messageClockId = setInterval(() => {
            if (this.roomId !== undefined) {
              let roomIndex = _self.findRoom(this.roomId);
              _self.rooms[roomIndex].messages.push({
                userId: this.id,
                time: new Date(),
                body: generators.sentence()
              });
            }
          }, 1000 * 2);
        }
      });
      this.users[this.users.length - 1].switchRooms();
      this.users[this.users.length - 1].sendMessage();
    }
  }

  addHumanUser(name, roomOrder) {
    let userIndex = this.findUserIndexByName(name);
    if (userIndex !== false) {
      console.log("found bastard, removing");
      this.users.splice(userIndex, 1);
    }

    let id = generators.uuid();
    this.users.push({
      id,
      name,
      roomId: this.rooms[this.findRoomByOrder(parseInt(roomOrder, 0))].id
    });
    return id;
  }

  sendHumanMessage(name, message) {
    let userIndex = this.findUserIndexByName(name);
    let userId = this.users[userIndex].id;
    let roomId = this.users[userIndex].roomId;
    let roomIndex = this.findRoom(roomId);
    this.rooms[roomIndex].messages.push({
      userId,
      time: new Date(),
      body: message
    });

    return this.rooms[roomIndex].messages;
  }

  init() {
    this.addRooms();
    this.addUsers();
  }
}

module.exports = new Room();

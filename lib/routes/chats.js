﻿const express = require("express");
const router = express.Router();
const chatController = require("../controllers/chat");

let join = (req, res, next) => {
  chatController.join(req, res, next);
};

let message = (req, res, next) => {
  chatController.message(req, res, next);
};

let getData = (req, res, next) => {
  chatController.getData(res, next);
};

// routes
router.post("/join", join);
router.post("/message", message);
router.get("/", getData);

module.exports = router;

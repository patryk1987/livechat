﻿module.exports = {
  port: process.env.NODE_ENV === "production" ? 80 : 8080,
  usersCount: process.env.USERS ? parseInt(process.env.USERS, 0) : 50
};

﻿const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const errorHandler = require("./lib/handlers/error");
const chatsRoute = require("./lib/routes/chats");
const config = require("./config");
const room = require("./lib/controllers/room");

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  next();
});

// api routes
app.use("/chats", chatsRoute);

// global error handler
app.use(errorHandler);

// init of chats simluation
room.init();

// start server
app.listen(config.port, function() {
  console.log("Server listening on port " + config.port);
});
